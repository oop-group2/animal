/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.animal;

/**
 *
 * @author Natthakritta
 */
public class Dog extends LandAnimal {

    private String nickname;

    public Dog(String nickname) {
        super("Dog", 4);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Dog " + nickname + " run");
    }

    @Override
    public void eat() {
        System.out.println("Dog " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog " + nickname + " speak Hong Hong");
    }

    @Override
    public void sleep() {
        System.out.println("Dog " + nickname + " sleep");
    }

}
