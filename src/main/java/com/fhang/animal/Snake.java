/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.animal;

/**
 *
 * @author Natthakritta
 */
public class Snake extends Reptile {

    public String nickname;

    public Snake(String nickname) {
        super("Snake", 0);
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Snake " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snake " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Snake " + nickname + " speak Fu Fu");
    }

    @Override
    public void sleep() {
        System.out.println("Snake " + nickname + " sleep");
    }

}
